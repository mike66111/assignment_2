package Program;

public class BubbleSort {
	private static int[] newSortInput;
	private static String answer;

	// this method is returned as a string since if 0 is involved it will always be
	// at the front meaning that it will not show in the int datatype
	// also it's going to need to be a string to be displayed to the user
	public static String bubbleSort(String sortInput) {

		// converts the string to an integer array each character being in a seperate
		// index
		newSortInput = new int[sortInput.length()];
		for (int i = 0; i < sortInput.length(); i++) {
			newSortInput[i] = sortInput.charAt(i) - '0';
		}

		// this nested for loop follows the algorithm to sort the array into accending
		// order
		for (int x = 0; x < (newSortInput.length - 1); x++) {
			for (int y = 0; y < (newSortInput.length - 1); y++) {
				if (newSortInput[y] > newSortInput[y + 1]) {
					int swap = newSortInput[y];
					newSortInput[y] = newSortInput[y + 1];
					newSortInput[y + 1] = swap;
				}

			}

		}
		// The array is sorted by this point but is not displayable to the user the code
		// underneath changes the array of integers concatinating it into a string
		answer = "";
		for (int i = 0; i < (newSortInput.length); i++) {
			String number = String.valueOf(newSortInput[i]);
			answer += number;
		}
		return answer;

	}

}
