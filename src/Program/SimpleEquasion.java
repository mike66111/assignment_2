package Program;
//importing framework
import java.math.BigDecimal;
import java.math.RoundingMode;

public class SimpleEquasion {
	//Declaring variables don't change
	private static double x;
	
	//sEquation returns a string value so that it can be shown to the user and so I don't have to convert it later
	public static String sEquasion(int digits, String num1, String num2, String num3) {
		//does a simple equation but converts the string values to double, the method accepts the strings from the text fields
		x = (Double.valueOf(num3) - Double.valueOf(num2)) / Double.valueOf(num1);
		//uses the method round taking the number of digits and the value of x
		String answer = String.valueOf(round(x, digits));
		return answer;

	}
	
	//returns a BigDecimal with the amount of decimal places being chose
	public static BigDecimal round(double value, int places) {

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd;

	}

}
