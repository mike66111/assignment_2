package Program;

//importing framework
import java.awt.CardLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.ParseException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.text.MaskFormatter;
import javax.swing.SwingConstants;

//I meant so you can use the buttons without clicking them with tab and enter like most other programs
//impliments actionlistener and keylistener
public class GUI implements ActionListener, KeyListener {
	// Variable defining DO NOT CHANGE
	private JFrame frmMathmaticalFunctions;
	private JPanel pnlMenu, pnlBubleSort, pnlSimpleEquation;
	private JButton btnBubblesort, btnSimpleEquation, btnMenu, btnMenu1, btnBubbleSort, btnExecute;
	private JLabel menu, lblSortAnswer, lblTheSortedNumber, lblBubbleSort, lblHowManyDecimal, lblX, lblXAnswer;
	private JFormattedTextField frmtdtxtfldInsertStudentId, ftfnum1, ftfnum2, ftfnum3;
	private static GUI window;
	private MaskFormatter formatter, mask, mask1, mask2;
	private JComboBox<?> cbDecimals;
	private JMenu mnNavigation;
	private JMenuItem mntmMenu, mntmSimpleEquation, mntmBubbleSort, mntmExit;
	private JMenu mnFile;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new GUI();
					window.frmMathmaticalFunctions.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// initialising the lookandfeel to look windows-like
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		// setting the frame
		frmMathmaticalFunctions = new JFrame();
		frmMathmaticalFunctions.setTitle("Mathmatical Functions");
		frmMathmaticalFunctions.setBounds(100, 100, 452, 377);
		frmMathmaticalFunctions.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// cardlayout set so that the panes can be "stacked" ontop of eachother
		frmMathmaticalFunctions.getContentPane().setLayout(new CardLayout(0, 0));

		// new JPanel
		pnlMenu = new JPanel();
		// Adding to contentPane
		frmMathmaticalFunctions.getContentPane().add(pnlMenu, "name_10562650552908");
		frmMathmaticalFunctions.getContentPane().addKeyListener(this);

		// Adding elements to pnlMenu
		btnBubblesort = new JButton("Bubble Sort");
		btnBubblesort.setFont(new Font("Tahoma", Font.PLAIN, 19));
		// adding action listener
		btnBubblesort.addActionListener(this);
		btnBubblesort.addKeyListener(this);

		btnSimpleEquation = new JButton("Simple Equation");
		btnSimpleEquation.setFont(new Font("Tahoma", Font.PLAIN, 19));
		// adding action listener
		btnSimpleEquation.addActionListener(this);
		btnSimpleEquation.addKeyListener(this);

		menu = new JLabel("Menu");
		// Grouping of elements in pnlMenu
		menu.setHorizontalAlignment(SwingConstants.CENTER);
		menu.setFont(new Font("Tahoma", Font.PLAIN, 34));
		GroupLayout gl_pnlMenu = new GroupLayout(pnlMenu);
		gl_pnlMenu
				.setHorizontalGroup(gl_pnlMenu.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_pnlMenu.createSequentialGroup()
								.addGroup(gl_pnlMenu.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_pnlMenu.createSequentialGroup().addGap(168)
												.addComponent(menu, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addGap(72))
										.addGroup(gl_pnlMenu.createSequentialGroup().addGap(103)
												.addGroup(gl_pnlMenu.createParallelGroup(Alignment.TRAILING)
														.addComponent(btnBubblesort, GroupLayout.DEFAULT_SIZE, 219,
																Short.MAX_VALUE)
														.addComponent(btnSimpleEquation, GroupLayout.DEFAULT_SIZE, 219,
																Short.MAX_VALUE))))
								.addGap(114)));
		gl_pnlMenu.setVerticalGroup(gl_pnlMenu.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_pnlMenu.createSequentialGroup().addContainerGap().addComponent(menu).addGap(35)
						.addComponent(btnSimpleEquation, GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE).addGap(31)
						.addComponent(btnBubblesort, GroupLayout.DEFAULT_SIZE, 68, Short.MAX_VALUE).addGap(69)));
		// setting layout to group layout on pnlMenu
		pnlMenu.setLayout(gl_pnlMenu);

		// initialising pnlBubbleSort
		pnlBubleSort = new JPanel();
		frmMathmaticalFunctions.getContentPane().add(pnlBubleSort, "name_10599353252426");

		// initialising the elements on pnlBubbleSort
		btnMenu = new JButton("Menu");
		btnMenu.addActionListener(this);
		btnMenu.addKeyListener(this);

		btnBubbleSort = new JButton("Bubble Sort");
		// adding action listener
		btnBubbleSort.addActionListener(this);
		btnBubbleSort.addKeyListener(this);

		lblTheSortedNumber = new JLabel("The sorted number is:");
		lblTheSortedNumber.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblSortAnswer = new JLabel("");
		lblSortAnswer.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblBubbleSort = new JLabel("Bubble Sort");
		lblBubbleSort.setHorizontalAlignment(SwingConstants.CENTER);
		lblBubbleSort.setFont(new Font("Tahoma", Font.PLAIN, 20));

		// initialising MaskFormatters used for formattedtextfields
		try {
			formatter = new MaskFormatter("#######");
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			mask = new MaskFormatter("*######");

		} catch (ParseException e) {
			e.printStackTrace();
		}
		// setting valid characters
		mask.setValidCharacters("-0123456789");

		try {
			mask1 = new MaskFormatter("*######");

		} catch (ParseException e) {
			e.printStackTrace();
		}
		// setting valid characters
		mask1.setValidCharacters("-0123456789");

		try {
			mask2 = new MaskFormatter("*######");

		} catch (ParseException e) {
			e.printStackTrace();
		}
		// setting valid characters
		mask2.setValidCharacters("-0123456789");

		// setting the placeholder character for formatter to white space so that I can
		// remove it later
		formatter.setPlaceholderCharacter(' ');
		// formattedTextField using the formatter mask
		frmtdtxtfldInsertStudentId = new JFormattedTextField(formatter);
		frmtdtxtfldInsertStudentId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		// setting the focus lost behaiviour so it commits the value when the button is
		// pressed
		frmtdtxtfldInsertStudentId.setFocusLostBehavior(JFormattedTextField.COMMIT);
		frmtdtxtfldInsertStudentId.addKeyListener(this);

		// grouping of elements in pnlBubbleSort
		JLabel lblInsertStudentId = new JLabel("Insert Student ID:");
		lblInsertStudentId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		GroupLayout gl_pnlBubleSort = new GroupLayout(pnlBubleSort);
		gl_pnlBubleSort.setHorizontalGroup(gl_pnlBubleSort.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_pnlBubleSort.createSequentialGroup().addGap(148).addGroup(gl_pnlBubleSort
						.createParallelGroup(Alignment.LEADING)
						.addComponent(lblBubbleSort, GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
						.addGroup(gl_pnlBubleSort.createSequentialGroup()
								.addComponent(btnMenu, GroupLayout.DEFAULT_SIZE, 115, Short.MAX_VALUE).addGap(12)))
						.addGap(161))
				.addGroup(
						gl_pnlBubleSort.createSequentialGroup().addContainerGap(264, Short.MAX_VALUE)
								.addComponent(btnBubbleSort, GroupLayout.PREFERRED_SIZE, 115,
										GroupLayout.PREFERRED_SIZE)
								.addGap(57))
				.addGroup(gl_pnlBubleSort.createSequentialGroup().addContainerGap().addGroup(gl_pnlBubleSort
						.createParallelGroup(Alignment.LEADING)
						.addComponent(lblTheSortedNumber, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_pnlBubleSort.createSequentialGroup().addGap(187).addComponent(lblSortAnswer,
								GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)))
						.addContainerGap(132, Short.MAX_VALUE))
				.addGroup(gl_pnlBubleSort.createSequentialGroup().addContainerGap()
						.addComponent(lblInsertStudentId, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(frmtdtxtfldInsertStudentId, GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
						.addGap(167)));
		gl_pnlBubleSort.setVerticalGroup(gl_pnlBubleSort.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_pnlBubleSort.createSequentialGroup().addGap(16)
						.addComponent(lblBubbleSort, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addGap(40)
						.addGroup(gl_pnlBubleSort.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblInsertStudentId, GroupLayout.PREFERRED_SIZE, 20,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(frmtdtxtfldInsertStudentId, GroupLayout.PREFERRED_SIZE, 25,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnBubbleSort, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addGap(46)
						.addGroup(gl_pnlBubleSort.createParallelGroup(Alignment.LEADING)
								.addComponent(lblTheSortedNumber, GroupLayout.PREFERRED_SIZE, 26,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblSortAnswer, GroupLayout.PREFERRED_SIZE, 26,
										GroupLayout.PREFERRED_SIZE))
						.addGap(42).addComponent(btnMenu, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)));
		// seting pnlBubbleSort to a group layout
		pnlBubleSort.setLayout(gl_pnlBubleSort);

		// Initialising pnlSimpleEquation
		pnlSimpleEquation = new JPanel();
		frmMathmaticalFunctions.getContentPane().add(pnlSimpleEquation, "name_16413372792932");

		// initialising the elements on pnlSimpleEquation
		btnMenu1 = new JButton("Menu");
		// adding actionListener
		btnMenu1.addActionListener(this);
		btnMenu1.addKeyListener(this);

		JLabel lblSimpleEquasion = new JLabel("Simple Equation");
		lblSimpleEquasion.setHorizontalAlignment(SwingConstants.CENTER);
		lblSimpleEquasion.setFont(new Font("Tahoma", Font.PLAIN, 20));

		// making an array to put into a combo box
		Integer places[] = { 1, 2, 3, 4, 5 };
		// combo box uses the places array
		cbDecimals = new JComboBox<>(places);
		cbDecimals.setFont(new Font("Tahoma", Font.PLAIN, 18));
		cbDecimals.addKeyListener(this);

		lblHowManyDecimal = new JLabel("How many decimal places:");
		lblHowManyDecimal.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblX = new JLabel("X=");
		lblX.setFont(new Font("Tahoma", Font.PLAIN, 18));

		lblXAnswer = new JLabel("");
		lblXAnswer.setFont(new Font("Tahoma", Font.PLAIN, 18));

		// ftfnum1 using the mask MaskFormatter
		ftfnum1 = new JFormattedTextField(mask);
		ftfnum1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		ftfnum1.setText("4");
		ftfnum1.setColumns(10);
		// commits the text when focus is lost
		ftfnum1.setFocusLostBehavior(JFormattedTextField.COMMIT);
		ftfnum1.addKeyListener(this);

		JLabel lblXplus = new JLabel("X+");
		lblXplus.setFont(new Font("Tahoma", Font.PLAIN, 18));

		// ftfnum2 using mask1 MaskFormatter
		ftfnum2 = new JFormattedTextField(mask1);
		ftfnum2.setText("6");
		ftfnum2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		ftfnum2.setColumns(10);
		// Commits the text when the focus is lost
		ftfnum2.setFocusLostBehavior(JFormattedTextField.COMMIT);
		ftfnum2.addKeyListener(this);

		JLabel lblEquals = new JLabel("=");
		lblEquals.setFont(new Font("Tahoma", Font.PLAIN, 18));

		// ftfnum3 using mask2 MaskFormatter
		ftfnum3 = new JFormattedTextField(mask2);
		ftfnum3.setText("8");
		ftfnum3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		ftfnum3.setColumns(10);
		// Commits the text when the focus is lost
		ftfnum3.setFocusLostBehavior(JFormattedTextField.COMMIT);
		ftfnum3.addKeyListener(this);

		btnExecute = new JButton("Execute");
		btnExecute.setFont(new Font("Tahoma", Font.PLAIN, 18));
		// adding action listener
		btnExecute.addActionListener(this);
		btnExecute.addKeyListener(this);
		// grouping the elements of pnlSimpleEquation
		GroupLayout gl_pnlSimpleEquation = new GroupLayout(pnlSimpleEquation);
		gl_pnlSimpleEquation.setHorizontalGroup(gl_pnlSimpleEquation.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_pnlSimpleEquation.createSequentialGroup().addGap(87)
						.addGroup(gl_pnlSimpleEquation.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_pnlSimpleEquation.createParallelGroup(Alignment.LEADING, false)
										.addGroup(gl_pnlSimpleEquation.createSequentialGroup()
												.addComponent(lblHowManyDecimal, GroupLayout.PREFERRED_SIZE, 224,
														GroupLayout.PREFERRED_SIZE)
												.addPreferredGap(ComponentPlacement.RELATED, 44, Short.MAX_VALUE))
										.addGroup(gl_pnlSimpleEquation.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED, 220, Short.MAX_VALUE)
												.addComponent(cbDecimals, GroupLayout.PREFERRED_SIZE, 48,
														GroupLayout.PREFERRED_SIZE)))
								.addGroup(gl_pnlSimpleEquation.createSequentialGroup()
										.addComponent(lblX, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(lblXAnswer,
												GroupLayout.PREFERRED_SIZE, 228, GroupLayout.PREFERRED_SIZE)))
						.addGap(292))
				.addGroup(gl_pnlSimpleEquation.createSequentialGroup().addGap(31)
						.addComponent(ftfnum1, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(lblXplus)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(ftfnum2, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(lblEquals)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(ftfnum3, GroupLayout.DEFAULT_SIZE, 116, Short.MAX_VALUE).addGap(245))
				.addGroup(Alignment.LEADING, gl_pnlSimpleEquation.createSequentialGroup().addGap(84)
						.addComponent(lblSimpleEquasion, GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE).addGap(286))
				.addGroup(Alignment.LEADING,
						gl_pnlSimpleEquation.createSequentialGroup().addGap(155)
								.addComponent(btnMenu1, GroupLayout.DEFAULT_SIZE, 109, Short.MAX_VALUE).addGap(383))
				.addGroup(Alignment.LEADING, gl_pnlSimpleEquation.createSequentialGroup().addGap(74)
						.addComponent(btnExecute, GroupLayout.DEFAULT_SIZE, 117, Short.MAX_VALUE).addGap(456)));
		gl_pnlSimpleEquation
				.setVerticalGroup(
						gl_pnlSimpleEquation.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_pnlSimpleEquation.createSequentialGroup().addContainerGap()
										.addComponent(lblSimpleEquasion, GroupLayout.PREFERRED_SIZE, 29,
												GroupLayout.PREFERRED_SIZE)
										.addGap(29)
										.addGroup(gl_pnlSimpleEquation.createParallelGroup(Alignment.BASELINE)
												.addComponent(ftfnum1, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblXplus)
												.addComponent(ftfnum2, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblEquals)
												.addComponent(ftfnum3, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
										.addGap(18)
										.addGroup(gl_pnlSimpleEquation.createParallelGroup(Alignment.LEADING)
												.addComponent(lblX, GroupLayout.PREFERRED_SIZE, 20,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														lblXAnswer, GroupLayout.PREFERRED_SIZE, 20,
														GroupLayout.PREFERRED_SIZE))
										.addGap(24)
										.addGroup(gl_pnlSimpleEquation.createParallelGroup(Alignment.LEADING, false)
												.addGroup(gl_pnlSimpleEquation.createSequentialGroup().addGap(4)
														.addComponent(lblHowManyDecimal, GroupLayout.PREFERRED_SIZE, 20,
																GroupLayout.PREFERRED_SIZE))
												.addComponent(cbDecimals, GroupLayout.PREFERRED_SIZE, 29,
														GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(btnExecute, GroupLayout.PREFERRED_SIZE, 45,
												GroupLayout.PREFERRED_SIZE)
										.addGap(30).addComponent(btnMenu1, GroupLayout.PREFERRED_SIZE, 29,
												GroupLayout.PREFERRED_SIZE)
										.addGap(19)));
		// group layout for pnlSimpleEquation
		pnlSimpleEquation.setLayout(gl_pnlSimpleEquation);

		// making the menu bar
		JMenuBar menuBar = new JMenuBar();
		frmMathmaticalFunctions.setJMenuBar(menuBar);

		mnFile = new JMenu("File (Alt+F)");
		// setting a keyboard mnemonic
		mnFile.setMnemonic(KeyEvent.VK_F);
		// adding mnFile to the menubar
		mnFile.addKeyListener(this);
		menuBar.add(mnFile);

		mnNavigation = new JMenu("Navigation (Alt+N)");
		// setting a keyboard mnemonic
		mnNavigation.setMnemonic(KeyEvent.VK_N);
		mnNavigation.addKeyListener(this);
		mnFile.add(mnNavigation);

		mntmMenu = new JMenuItem("Menu (Alt+M)");
		// adding an actionlistener
		mntmMenu.addActionListener(this);
		// setting a keyboard mnemonic
		mntmMenu.setMnemonic(KeyEvent.VK_M);
		// adding mnNavigation to mntmMenu
		mnNavigation.add(mntmMenu);

		mntmSimpleEquation = new JMenuItem("Simple Equasion (Alt+S)");
		// setting a keyboard mnemonic
		mntmSimpleEquation.setMnemonic(KeyEvent.VK_S);
		// adding an action listener
		mntmSimpleEquation.addActionListener(this);
		// adding mntmSimpleEquation to mnNavigation
		mnNavigation.add(mntmSimpleEquation);

		mntmBubbleSort = new JMenuItem("Bubble Sort (Alt+B)");
		// setting a keyboard mnemonic
		mntmBubbleSort.setMnemonic(KeyEvent.VK_B);
		// adding action listener
		mntmBubbleSort.addActionListener(this);
		// adding mntmBubbleSort to mnNavigation
		mnNavigation.add(mntmBubbleSort);

		// adding a seperator to mnFile
		mnFile.addSeparator();
		mntmExit = new JMenuItem("Exit (Alt+E)");
		// adding an action listener
		mntmExit.addActionListener(this);
		// setting a keyboard mnemonic
		mntmExit.setMnemonic(KeyEvent.VK_E);
		mnFile.add(mntmExit);

	}

	// setting up the actionPerformed to run code when the actionListeners have been
	// triggered
	public void actionPerformed(ActionEvent e) {
		// the buttons to move to pnlBubleSort that has the BubbleSort method
		if (e.getSource() == btnBubblesort || e.getSource() == mntmBubbleSort) {
			// removes everything from the contentPane
			frmMathmaticalFunctions.getContentPane().removeAll();
			// adds pnlBubbleSort
			frmMathmaticalFunctions.getContentPane().add(pnlBubleSort);
			// validates the frame
			frmMathmaticalFunctions.validate();
			// repaints the frame
			frmMathmaticalFunctions.repaint();
			// sets the panel back to the default
			frmtdtxtfldInsertStudentId.setText("");
			lblSortAnswer.setText("");
			// setting the focus so you can tab between the elements
			frmtdtxtfldInsertStudentId.requestFocus();
		}
		// the buttons to move to pnlSimpleEquation that has the SimpleEquation method
		else if (e.getSource() == btnSimpleEquation || e.getSource() == mntmSimpleEquation) {
			// removes everything from the contentPane
			frmMathmaticalFunctions.getContentPane().removeAll();
			// adds pnlSimpleEquation
			frmMathmaticalFunctions.getContentPane().add(pnlSimpleEquation);
			// validates the frame
			frmMathmaticalFunctions.validate();
			// repaints the frame
			frmMathmaticalFunctions.repaint();
			// sets the answer lable and the text fields back to the default value
			ftfnum1.setText("4");
			ftfnum2.setText("6");
			ftfnum3.setText("8");
			cbDecimals.setSelectedIndex(0);
			lblXAnswer.setText(SimpleEquasion.sEquasion((int) cbDecimals.getSelectedItem(), ftfnum1.getText(),
					ftfnum2.getText(), ftfnum3.getText()));
			// setting the focus so you can tab between the elements
			ftfnum1.requestFocus();
		}
		// the buttons to move to pnlMenu that acts as the menu
		else if (e.getSource() == btnMenu || e.getSource() == mntmMenu || e.getSource() == btnMenu1) {
			frmMathmaticalFunctions.getContentPane().removeAll();
			frmMathmaticalFunctions.getContentPane().add(pnlMenu);
			frmMathmaticalFunctions.validate();
			frmMathmaticalFunctions.repaint();
			// setting the focus on the button so you can tab through
			btnSimpleEquation.requestFocus();

		}
		// runs if btnBubbleSort is pressed
		else if (e.getSource() == btnBubbleSort) {
			// if the text in frmtdtxtfldInsertStudentId with all white space deleted is 7
			// characters long then it will run the code underneath
			if ((frmtdtxtfldInsertStudentId.getText().replaceAll("\\s", "")).length() == 7) {
				// sets the text of lblSortAnswer to be the return from the bubbleSort method
				// with the text from frmtdtxtfldInsertStudentId being the argument
				lblSortAnswer.setText(BubbleSort.bubbleSort(frmtdtxtfldInsertStudentId.getText()));

			} else {
				// displays a message in a promt to explain that they need to insert 7 digits
				// will only display if they don't enter 7
				JOptionPane.showMessageDialog(null, "please enter a valid student ID (7digits)");
				lblSortAnswer.setText("");
			}
		}
		// runs when the execute button is pressed
		else if (e.getSource() == btnExecute) {
			// trys to run a method which parses a double value to check is a number has
			// been entered into all of the text fields so that it won't try to run the
			// equation with blank values
			try {

				Double.parseDouble(ftfnum1.getText());
				Double.parseDouble(ftfnum2.getText());
				Double.parseDouble(ftfnum3.getText());
			} catch (Exception e1) {
				// when the exeption is caught display a message and break from the method by
				// returning nothing
				JOptionPane.showMessageDialog(null, "Please enter valid numbers for the calculation");
				return;
			}
			// making sure that the code won't perform an arithmetic error making it so you
			// can't enter 0x as dividing by 0 won't work
			if (Integer.valueOf(ftfnum1.getText().replaceAll("\\s", "")) == 0) {

				JOptionPane.showMessageDialog(null, "Please enter valid numbers for the calculation (The value of x can't be 0)");
			} else {
				// changes lblXAnswer to the return of SimpleEquasion using the textfields and
				// the combobox as the arguments to be passed
				lblXAnswer.setText(SimpleEquasion.sEquasion((int) cbDecimals.getSelectedItem(),
						ftfnum1.getText().replaceAll("\\s", ""), ftfnum2.getText().replaceAll("\\s", ""),
						ftfnum3.getText().replaceAll("\\s", "")));
			}
		}
		// runs when mntmExit is pressed
		else if (e.getSource() == mntmExit) {
			// shows a confirmation box if you select yes the program will close
			int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit the program?");
			if (dialogResult == JOptionPane.YES_OPTION) {
				System.exit(0);
			}
		}

		// mouse adapter on frmtdtxtfldInsertStudentId when it is clicked when the box
		// is empty the caret will go to the start of the box
		frmtdtxtfldInsertStudentId.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				// when the text box is empty the string will be 7 spaces as the mask is 7
				// spaces long
				if (frmtdtxtfldInsertStudentId.getText().equals("       "))
					frmtdtxtfldInsertStudentId.setCaretPosition(0);
			}
		});

	}

	// key shortcuts so that you can press the menu items without having to go
	// through it if you know the mnemonic by memory
	@Override
	public void keyPressed(KeyEvent e3) {
		if (e3.getKeyCode() == KeyEvent.VK_M && e3.isAltDown()) {
			frmMathmaticalFunctions.getContentPane().removeAll();
			frmMathmaticalFunctions.getContentPane().add(pnlMenu);
			frmMathmaticalFunctions.validate();
			frmMathmaticalFunctions.repaint();
			btnSimpleEquation.requestFocus();
		} else if (e3.getKeyCode() == KeyEvent.VK_B && e3.isAltDown()) {
			// removes everything from the contentPane
			frmMathmaticalFunctions.getContentPane().removeAll();
			// adds pnlBubbleSort
			frmMathmaticalFunctions.getContentPane().add(pnlBubleSort);
			// validates the frame
			frmMathmaticalFunctions.validate();
			// repaints the frame
			frmMathmaticalFunctions.repaint();
			// sets the panel back to the default
			frmtdtxtfldInsertStudentId.setText("");
			lblSortAnswer.setText("");
			frmtdtxtfldInsertStudentId.requestFocus();
		} else if (e3.getKeyCode() == KeyEvent.VK_S && e3.isAltDown()) {
			// removes everything from the contentPane
			frmMathmaticalFunctions.getContentPane().removeAll();
			// adds pnlSimpleEquation
			frmMathmaticalFunctions.getContentPane().add(pnlSimpleEquation);
			// validates the frame
			frmMathmaticalFunctions.validate();
			// repaints the frame
			frmMathmaticalFunctions.repaint();
			// sets the answer lable and the text fields back to the default value
			ftfnum1.setText("4");
			ftfnum2.setText("6");
			ftfnum3.setText("8");
			cbDecimals.setSelectedIndex(0);
			lblXAnswer.setText(SimpleEquasion.sEquasion((int) cbDecimals.getSelectedItem(), ftfnum1.getText(),
					ftfnum2.getText(), ftfnum3.getText()));
			ftfnum1.requestFocus();
		} else if (e3.getKeyCode() == KeyEvent.VK_E && e3.isAltDown()) {
			int dialogResult = JOptionPane.showConfirmDialog(null, "Are you sure you want to exit the program?");
			if (dialogResult == JOptionPane.YES_OPTION) {
				System.exit(0);
			}
		}

	}

	// method used to parse double
	public static boolean checkIfDouble(String number) {

		try {

			Double.parseDouble(number);

			return true;

		} catch (Exception e) {
			return false;
		}
	}

	// these two methods are here because keylistener is implimented it needed these
	// 2 to run without erroring.
	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

}
