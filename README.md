# Background
The aim of the assignment is to develop a console application in Java programming
language that performs a number of mathematical functions. The functions should be
menu driven, i.e. a number of options will be displayed, and the user will be able to
input what function of the package they want to run. On selecting a mathematical
function to run, the program should then run that function, and then return to the main
menu.
---
# Required Task	
Students are required to develop a console application/program that performs a number of
mathematical functions; and 1250 �10%-word report on the procedural program design and
approach taken to implement the functions.
You will be assessed by: how much functionality has been implemented; the implementation
quality. Your program will consist of the following functions in table 1. There are 65 marks
available for the program code part of the assessment, and 35 mark for the report.
---
# Function Descriptions
1. Menu The program should display a menu, with a user
selectable choice for each part of program that has
been implemented. The program should perform input
validation (e.g. detect incorrect user entries and input)
and also have an option to exit the program. On
selecting a menu item, the appropriate part of the
program should run, and the menu should then be
redisplayed (unless exit has been selected). If a part of
the program has not been implemented (e.g., Simple
equation), the program should simply display a short
message, e.g. "Function not implemented choose from
other options".


2. Accuracy Option An option should be implemented to allow the user to
specify to how many decimal places the numerical
results should be displayed. This value should be
restricted to a range between 1 and 5 decimals
(inclusive). This accuracy option should apply to any
numerical result (from the functions) displayed to the
user.

3. Simple Equation

4. The Bubble Sort algorithm
The bubble sort algorithm is one of many algorithms that
can be used to sort a list into ascending order.




